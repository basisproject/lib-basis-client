"use strict";

const standard_api = require('../helpers/standard-api');

const {list, get} = standard_api.generate('/cost-tags', 'basis.cost_tag.CostTag');
exports.list = list;
exports.get = get;

