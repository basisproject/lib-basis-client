"use strict";

const Promise = require('bluebird');
const Exonum = require('exonum-client');
const rp = require('request-promise');
const protobuf = require('./protobuf');
const config = require('./config').config;

const types = {};
const message_id_map = (function() {
	// these MUST be ordered in the same way as basis/src/block/transactions/mod.rs
	const transactions = [
		'user.TxCreate',
		'user.TxUpdate',
		'user.TxSetPubkey',
		'user.TxSetRoles',
		'user.TxDelete',

		'company.TxCreatePrivate',
		'company.TxUpdate',
		'company.TxSetType',
		'company.TxDelete',

		'company_member.TxCreate',
		'company_member.TxUpdate',
		'company_member.TxDelete',

		'labor.TxCreate',
		'labor.TxUpdate',

		'product.TxCreate',
		'product.TxUpdate',
		'product.TxDelete',

		'resource_tag.TxCreate',
		'resource_tag.TxDelete',

		'order.TxCreate',
		'order.TxUpdateStatus',
		'order.TxUpdateCostTags',

		'cost_tag.TxCreate',
		'cost_tag.TxUpdate',
		'cost_tag.TxDelete',
	];
	const map = {};
	let i = 0;
	transactions.forEach((t) => { map[t] = i++; });
	return map;
})();

Object.keys(message_id_map).forEach((key) => {
	const [type, tx] = key.split('.');
	if(!types[type]) types[type] = {};
	types[type][tx] = {
		type: `basis.${key}`,
		msg_id: message_id_map[key],
	}
});

exports.types = types;

exports.make = (type, data, params) => {
	const Transaction = protobuf.root.lookupType(type.type);
	const map_types = {
		'google.protobuf.Timestamp': 'Timestamp',
		'exonum.Hash': 'Hash',
		'exonum.PublicKey': 'Pubkey',
		'CompanyType': 'CompanyType',
		'Product.Unit': 'Unit',
		'Order.ProcessStatus': 'ProcessStatus',
	};
	Object.keys(Transaction.fields).forEach((field) => {
		if(typeof(data[field]) == 'undefined') return;
		const spec = Transaction.fields[field];
		const mapping = map_types[spec.type];
		if(!mapping) return;
		data[field] = protobuf.types[mapping].gen(data[field]);
	});
	const errmsg = Transaction.verify(data);
	if(errmsg) {
		throw new Error(`transaction: ${type}: verification error: ${errmsg}`);
	}
	const trans = Exonum.newTransaction({
		schema: Transaction,
		author: params.pubkey,
		service_id: params.service_id || config.service_id,
		message_id: params.message_id || type.msg_id,
	});
	return trans;
};

exports.send = async (type, data, params) => {
	data = JSON.parse(JSON.stringify(data));
	const trans = exports.make(type, data, params);
	let res = await trans.send(`${config.endpoint}/explorer/v1/transactions`, data, params.privkey, 0);
	if(res instanceof Error) {
		throw res;
	}
	return res;
};

const whoswho = {};
exports.add_user = (who, pubkey, seckey) => {
	whoswho[who] = {pub: pubkey, sec: seckey};
};

exports.clear_users = () => {
	Object.keys(whoswho).forEach((key) => delete whoswho[key]);
};

exports.send_as = async (who, type, data, params, options) => {
	options || (options = {});
	const user = whoswho[who];
	if(!user) throw new Error(`helpers/transactions::send_as() -- missing user ${who}`);
	const newparams = Object.assign({}, {
		pubkey: user.pub,
		privkey: user.sec,
	}, params || {});
	let txid = await exports.send(type, data, newparams);
	if(options.no_wait) {
		return txid;
	}
	let status = await exports.wait(txid, options);
	status.txid = txid;
	return status;
};

exports.get = async (txid, options) => {
	options || (options = {});
	try {
		const res = await rp({
			url: `${config.endpoint}/explorer/v1/transactions?hash=${txid}`,
			json: true,
		});
		return res;
	} catch(err) {
		if(err.statusCode && err.statusCode === 404 && !options.throw_missing) {
			return null;
		}
		throw err;
	}
};

function extract_status(trans) {
	return {
		committed: trans.type == 'committed',
		success: trans.status.type == 'success',
		code: trans.status.code,
		description: trans.status.description,
	}
}

exports.wait = async (txid, options) => {
	options || (options = {});
	let timeout = false;
	setTimeout(() => { timeout = true; }, options.timeout || 30000);
	let trans = null;
	while(true) {
		try {
			const res = await exports.get(txid);
			if(timeout) {
				if(options.raw) {
					throw new Error(`helpers/transactions::wait() -- timeout: ${txid}`);
				}
				return {missing: true};
			}
			if(res && res.type != 'unknown') {
				trans = res;
				break;
			}
		} catch(err) {
			console.log('>>> trans.wait() -- ', err);
			throw err;
		}
		await Promise.delay(500);
	}
	if(options.raw) return trans;
	return extract_status(trans);
};

exports.status = async (txid) => {
	const res = await exports.get(txid);
	if(!res || res.type == 'unknown') return {missing: true};
	return extract_status(res);
};

