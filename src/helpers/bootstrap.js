"use strict";

const Promise = require('bluebird');
const config = require('./config').config;
const trans = require('./transactions');
const tx = trans.types;
const Users = require('../models/users');

exports.load = async function() {
	const data = {
		id: config.bootstrap_user.id,
		pubkey: config.bootstrap_user.pub,
		roles: ['SuperAdmin'],
		email: config.bootstrap_user.email,
		name: config.bootstrap_user.name,
		meta: '{}',
		created: new Date().toISOString(),
	};
	const params = {
		pubkey: config.bootstrap_user.pub,
		privkey: config.bootstrap_user.sec,
	};
	const txid = await trans.send(tx.user.TxCreate, data, params);
	const status = await trans.wait(txid);
	if(!status.success) {
		throw new Error('helpers/bootstrap::load() -- user create failed: '+JSON.stringify(status));
	}
	return txid;
};

exports.unload = async function() {
	const data = {
		id: config.bootstrap_user.id,
		memo: 'You are *fired* =D!!',
		deleted: new Date().toISOString(),
	};
	const params = {
		pubkey: config.bootstrap_user.pub,
		privkey: config.bootstrap_user.sec,
		message_id: 4,
	};
	const txid = await trans.send(tx.user.TxDelete, data, params);
	const status = await trans.wait(txid);
	if(!status.success) {
		throw new Error('helpers/bootstrap::unload() -- user delete failed: '+JSON.stringify(status));
	}
};

