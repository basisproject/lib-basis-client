"use strict";

const fs = require('fs');
const config = require('./helpers/config');
const protobuf = require('./helpers/protobuf');
const models = {};

let has_init = false;
exports.init = function(configdata) {
	if(has_init) return;
	config.init(configdata);
	protobuf.load();

	const modeldir = __dirname + '/models';
	fs.readdirSync(modeldir).forEach(function(file) {
		if(file.match(/^\./) || !file.match(/\.js$/)) return;
		const model = require(modeldir + '/'+ file);
		const slug = file.replace(/\.js/, '').replace(/-/g, '_');
		models[slug] = model;
	});
	has_init = true;
};

exports.config = config.config;
exports.protobuf = protobuf;
exports.models = models;
exports.transactions = require('./helpers/transactions');
exports.bootstrap = require('./helpers/bootstrap');
exports.standard_api = require('./helpers/standard-api');

